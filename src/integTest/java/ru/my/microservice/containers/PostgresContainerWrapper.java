package ru.my.microservice.containers;

import org.testcontainers.containers.PostgreSQLContainer;

public class PostgresContainerWrapper extends PostgreSQLContainer<PostgresContainerWrapper>{
    private static final String POSTGRES_VERSION = "postgres:14";
    private static final String POSTGRES_DB = "integtest_alfanet_tests_db";
    private static final String POSTGRES_USER = "test";
    private static final String POSTGRES_PASSWORD = "test";

    private static PostgresContainerWrapper container;
    private PostgresContainerWrapper () {
        super (POSTGRES_VERSION);
        this
                .withDatabaseName(POSTGRES_DB)
                .withUsername(POSTGRES_USER)
                .withPassword(POSTGRES_PASSWORD);
    }

    public static PostgresContainerWrapper getInstance(){
        if (container == null){
            container = new PostgresContainerWrapper();
        }
        return container;
    }

    @Override
    public void start(){
        super.start();
    }
}
