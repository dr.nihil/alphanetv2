package ru.my.microservice.moduletests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.my.microservice.containers.PostgresContainerWrapper;
import ru.my.microservice.dblayer.entities.User;
import ru.my.microservice.services.UserService;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserServiceTest_PostgresContainer {
    @Container
    private static final PostgresContainerWrapper postgresContainer = PostgresContainerWrapper.getInstance();

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);
    }

    @Autowired
    private UserService userService;

    @Test
    public void createUserTest() {
        User user = new User("testuser");
        user.setFirstname("test_firstname");
        user.setLastname("test_lasName");
        User newUser = userService.save(user);
        Assertions.assertNotNull(newUser.getId());
        Assertions.assertEquals(user.getNickname(), newUser.getNickname());
    }
}
