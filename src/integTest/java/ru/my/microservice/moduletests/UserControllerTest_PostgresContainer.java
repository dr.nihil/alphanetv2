package ru.my.microservice.moduletests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.server.ResponseStatusException;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.my.microservice.containers.PostgresContainerWrapper;
import ru.my.microservice.controllers.dto.ContactDTO;
import ru.my.microservice.controllers.dto.PictureDTO;
import ru.my.microservice.controllers.dto.SkillDTO;
import ru.my.microservice.controllers.dto.UserDTO;
import ru.my.microservice.dblayer.entities.Picture;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest_PostgresContainer {
    @Container
    private static final PostgresContainerWrapper postgresContainer = PostgresContainerWrapper.getInstance();
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    JdbcTemplate jdbcTemplate;
    private final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX").create();
    private static final String USER_NOT_FOUND = "User with id %s not found";
    private static final String TRUNCATE_TABLES = "TRUNCATE TABLE city, contact, pic, skill, user_skills, subscription, \"user\" CASCADE";

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);
    }

    @BeforeEach
    private void cleanDatabase() {
        jdbcTemplate.execute(TRUNCATE_TABLES);
    }

    @Test
    void createUser_success() throws Exception {
        String userJson = readFileFromResources("test_user.json");
        UserDTO user = gson.fromJson(userJson, UserDTO.class);

        ResultActions resultActions = mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(userJson));

        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.nickname").value(user.getNickname()))
                .andExpect(jsonPath("$.firstname").value(user.getFirstname()))
                .andExpect(jsonPath("$.lastname").value(user.getLastname()))
                .andExpect(jsonPath("$.midlename").value(user.getMidlename()))
                .andExpect(jsonPath("$.addInfo").value(user.getAddInfo()))
                .andExpect(jsonPath("$.city.name").value(user.getCity().getName()))
                .andExpect(jsonPath("$.contacts.length()").value(user.getContacts().size()))
                .andExpect(jsonPath("$.skills.length()").value(user.getSkills().size()));
    }

    @Test
    void getUser_success() throws Exception {
        String userJson = readFileFromResources("test_user.json");
        String responseJson = mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(userJson))
                .andReturn().getResponse().getContentAsString();

        UserDTO user = gson.fromJson(responseJson, UserDTO.class);

        mockMvc.perform(get("/users/{id}", user.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nickname").value(user.getNickname()))
                .andExpect(jsonPath("$.firstname").value(user.getFirstname()))
                .andExpect(jsonPath("$.lastname").value(user.getLastname()))
                .andExpect(jsonPath("$.midlename").value(user.getMidlename()))
                .andExpect(jsonPath("$.addInfo").value(user.getAddInfo()))
                .andExpect(jsonPath("$.city.name").value(user.getCity().getName()))
                .andExpect(jsonPath("$.contacts.length()").value(user.getContacts().size()))
                .andExpect(jsonPath("$.skills.length()").value(user.getSkills().size()));

    }

    @Test
    void getUser_error() throws Exception {
        int userId = 1;

        mockMvc.perform(get("/users/{id}", userId))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(String.format(USER_NOT_FOUND, userId))));
    }

    @Test
    void getUsers() throws Exception {
        int userCount = 5;
        createSimpleUsers(userCount);
        int max = 10;
        int offset = 0;

        mockMvc.perform(get("/users?max={max}&offset={offset}", max, offset))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(userCount))
                .andExpect(jsonPath("$[*].nickname").value(containsInAnyOrder("usertest0", "usertest1", "usertest2", "usertest3", "usertest4")));
    }

    @Test
    void deleteUser_soft() throws Exception {
        String userJson = readFileFromResources("test_user.json");
        String responseJson = mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(userJson))
                .andReturn().getResponse().getContentAsString();

        UserDTO user = gson.fromJson(responseJson, UserDTO.class);

        mockMvc.perform(delete("/users/{id}", user.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", equalTo("User with id " + user.getId() + " is deleted")));

        mockMvc.perform(get("/users/{id}", user.getId()))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(String.format(USER_NOT_FOUND, user.getId()))));
    }

    @Test
    void deleteUser_hard() throws Exception {
        String userJson = readFileFromResources("test_user.json");
        String responseJson = mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(userJson))
                .andReturn().getResponse().getContentAsString();

        UserDTO user = gson.fromJson(responseJson, UserDTO.class);

        mockMvc.perform(delete("/users/{id}?hard=true", user.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", equalTo("User with id " + user.getId() + " is deleted")));

        mockMvc.perform(get("/users/{id}", user.getId()))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(String.format(USER_NOT_FOUND, user.getId()))));
    }

    @Test
    void deleteUser_error() throws Exception {
        long userId = 1;

        mockMvc.perform(delete("/users/{id}", userId))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(String.format(USER_NOT_FOUND, userId))));
    }

    @Test
    void updateUser_success() throws Exception {
        String responseJson = mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(readFileFromResources("test_user.json")))
                .andReturn().getResponse().getContentAsString();

        UserDTO user = gson.fromJson(responseJson, UserDTO.class);
        user.setFirstname("updated user");
        user.getSkills().add(new SkillDTO("Python"));
        user.getContacts().add(new ContactDTO("email", "ma@ma"));

        mockMvc.perform(put("/users/{id}", user.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(gson.toJson(user)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(user.getId()))
                .andExpect(jsonPath("$.nickname").value(user.getNickname()))
                .andExpect(jsonPath("$.firstname").value(user.getFirstname()))
                .andExpect(jsonPath("$.lastname").value(user.getLastname()))
                .andExpect(jsonPath("$.midlename").value(user.getMidlename()))
                .andExpect(jsonPath("$.addInfo").value(user.getAddInfo()))
                .andExpect(jsonPath("$.city.name").value(user.getCity().getName()))
                .andExpect(jsonPath("$.contacts.length()").value(user.getContacts().size()))
                .andExpect(jsonPath("$.skills.length()").value(user.getSkills().size()));
    }

    @Test
    void updateUser_error() throws Exception {
        UserDTO user = new UserDTO();
        user.setNickname("testNickname");
        user.setId(1);

        mockMvc.perform(put("/users/{id}", user.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(gson.toJson(user)))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(String.format(USER_NOT_FOUND, user.getId()))));
    }

    @Test
    void addPictureToUser_success() throws Exception {
        UserDTO user = createSimpleUsers(1).get(0);

        PictureDTO picture = new PictureDTO();
        picture.setImage("image".getBytes());
        picture.setActual(true);
        mockMvc.perform(post("/users/{id}/pic", user.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(gson.toJson(picture)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].actual").value(true));
    }

    @Test
    void addPictureToUser_error() throws Exception {
        long userId = 1;
        mockMvc.perform(post("/users/{id}/pic", userId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(new Picture("image".getBytes(), true))))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(String.format(USER_NOT_FOUND, userId))));
    }

    @Test
    void getActivePicture_success() throws Exception {
        UserDTO user = createSimpleUsers(1).get(0);

        mockMvc.perform(post("/users/{id}/pic", user.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(gson.toJson(new Picture("image".getBytes(), true))));

        mockMvc.perform(get("/users/{id}/pic", user.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.actual").value(true));
    }

    @Test
    void getActivePicture_error() throws Exception {
        long userId = 1;

        mockMvc.perform(get("/users/{id}/pic", userId))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(String.format(USER_NOT_FOUND, userId))));
    }

    @Test
    void createSubscriptionToUser_success() throws Exception {
        List<UserDTO> users = createSimpleUsers(2);

        mockMvc.perform(post("/users/{id}/subscriptions?toUserId={toUserId}", users.get(0).getId(), users.get(1).getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].nickname").value(users.get(1).getNickname()));
    }

    @Test
    void createSubscriptionToUser_error() throws Exception {
        long userId1 = 1;
        long userId2 = 2;

        mockMvc.perform(post("/users/{id}/subscriptions?toUserId={toUserId}", userId1, userId2))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(String.format(USER_NOT_FOUND, userId1))));
    }

    @Test
    void deleteSubscription_success() throws Exception {
        List<UserDTO> users = createSimpleUsers(2);

        mockMvc.perform(post("/users/{id}/subscriptions?toUserId={toUserId}", users.get(0).getId(), users.get(1).getId()));

        mockMvc.perform(delete("/users/{id}/subscriptions?toUserId={toUserId}", users.get(0).getId(), users.get(1).getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    void deleteSubscription_error() throws Exception {
        long userId1 = 1;
        long userId2 = 2;

        mockMvc.perform(delete("/users/{id}/subscriptions?toUserId={toUserId}", userId1, userId2))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(String.format(USER_NOT_FOUND, userId1))));
    }

    @Test
    void getSubscriptions_succces() throws Exception {
        List<UserDTO> users = createSimpleUsers(2);

        mockMvc.perform(post("/users/{id}/subscriptions?toUserId={toUserId}", users.get(0).getId(), users.get(1).getId()));

        mockMvc.perform(get("/users/{id}/subscriptions", users.get(0).getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].nickname").value(users.get(1).getNickname()));
    }

    @Test
    void getSubscriptions_error() throws Exception {
        long userId = 1;

        mockMvc.perform(get("/users/{id}/subscriptions", userId))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(String.format(USER_NOT_FOUND, userId))));
    }

    @Test
    void getSubscriptors_success() throws Exception {
        List<UserDTO> users = createSimpleUsers(2);

        mockMvc.perform(post("/users/{id}/subscriptions?toUserId={toUserId}", users.get(0).getId(), users.get(1).getId()));

        mockMvc.perform(get("/users/{id}/subscriptors", users.get(1).getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].nickname").value(users.get(0).getNickname()));
    }

    @Test
    void getSubscriptors_error() throws Exception {
        long userId = 1;

        mockMvc.perform(get("/users/{id}/subscriptors", userId))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(String.format(USER_NOT_FOUND, userId))));
    }

    private String readFileFromResources(String filename) throws URISyntaxException, IOException {
        URL resource = this.getClass().getClassLoader().getResource(String.format("%s/%s", this.getClass().getSimpleName(), filename));
        byte[] bytes = Files.readAllBytes(Paths.get(resource.toURI()));
        return new String(bytes);
    }

    private List<UserDTO> createSimpleUsers(int count) throws Exception {
        List<UserDTO> users = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            UserDTO user = new UserDTO();
            user.setNickname("usertest" + i);
            String userJson = mockMvc.perform(post("/users")
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(gson.toJson(user)))
                    .andReturn().getResponse().getContentAsString();
            users.add(gson.fromJson(userJson, UserDTO.class));
        }
        return users;
    }
}
