package ru.my.microservice.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.server.ResponseStatusException;
import ru.my.microservice.dblayer.entities.Picture;
import ru.my.microservice.dblayer.entities.User;
import ru.my.microservice.services.exceptions.ObjectNotFoundException;
import ru.my.microservice.services.UserService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;
    @SpyBean
    private ModelMapper modelMapper;

    private final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX").create();

    @Test
    void createUser_success() throws Exception {
        String userJson = readFileFromResources("test_user.json");
        User user = gson.fromJson(userJson, User.class);
        Mockito.when(userService.save(Mockito.any(User.class))).thenReturn(user);

        ResultActions resultActions = mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(userJson));

        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.nickname").value(user.getNickname()))
                .andExpect(jsonPath("$.firstname").value(user.getFirstname()))
                .andExpect(jsonPath("$.lastname").value(user.getLastname()))
                .andExpect(jsonPath("$.midlename").value(user.getMidlename()))
                .andExpect(jsonPath("$.addInfo").value(user.getAddInfo()))
                .andExpect(jsonPath("$.city.name").value(user.getCity().getName()))
                .andExpect(jsonPath("$.contacts.length()").value(user.getContacts().size()))
                .andExpect(jsonPath("$.skills.length()").value(user.getSkills().size()));
    }

    @Test
    void getUser_success() throws Exception {
        String userJson = readFileFromResources("test_user.json");
        User user = gson.fromJson(userJson, User.class);
        user.setId(1);
        Mockito.when(userService.getUserById(user.getId())).thenReturn(user);

        ResultActions resultActions = mockMvc.perform(get("/users/{id}", user.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nickname").value(user.getNickname()))
                .andExpect(jsonPath("$.firstname").value(user.getFirstname()))
                .andExpect(jsonPath("$.lastname").value(user.getLastname()))
                .andExpect(jsonPath("$.midlename").value(user.getMidlename()))
                .andExpect(jsonPath("$.addInfo").value(user.getAddInfo()))
                .andExpect(jsonPath("$.city.name").value(user.getCity().getName()))
                .andExpect(jsonPath("$.contacts.length()").value(user.getContacts().size()))
                .andExpect(jsonPath("$.skills.length()").value(user.getSkills().size()));

    }

    @Test
    void getUser_error() throws Exception {
        User user = new User("testNickname");
        user.setId(1);
        String errorText = "User with id " + user.getId() + " not found";
        Mockito.when(userService.getUserById(user.getId())).thenThrow(new ObjectNotFoundException(errorText));

        ResultActions resultActions = mockMvc.perform(get("/users/{id}", user.getId()))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(errorText)));
    }

    @Test
    void getUsers() throws Exception {
        List<User> users = new ArrayList<>();
        int userCount = 5;
        for (int i = 0; i < userCount; i++) {
            users.add(new User("nickname" + i));
        }
        int max = 10;
        int offset = 0;
        Mockito.when(userService.getUsers(max, offset)).thenReturn(users);

        ResultActions resultActions = mockMvc.perform(get("/users?max={max}&offset={offset}", max, offset))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(users.size()))
                .andExpect(jsonPath("$[*].nickname").value(containsInAnyOrder("nickname0", "nickname1", "nickname2", "nickname3", "nickname4")));
    }

    @Test
    void deleteUser_success() throws Exception {
        User user = new User("testNickname");
        user.setId(1);
        Mockito.when(userService.delete(user.getId(), false)).thenReturn(true);

        ResultActions resultActions = mockMvc.perform(delete("/users/{id}", user.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", equalTo("User with id " + user.getId() + " is deleted")));
    }

    @Test
    void deleteUser_error() throws Exception {
        User user = new User("testNickname");
        user.setId(1);
        String errorText = "User with id " + user.getId() + " not found";
        Mockito.when(userService.delete(user.getId(), false)).thenThrow(new ObjectNotFoundException(errorText));

        ResultActions resultActions = mockMvc.perform(delete("/users/{id}", user.getId()))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(errorText)));
    }

    @Test
    void updateUser_success() throws Exception {
        User user = gson.fromJson(readFileFromResources("test_user.json"), User.class);
        user.setId(1);
        Mockito.when(userService.update(Mockito.any(User.class))).thenReturn(user);

        ResultActions resultActions = mockMvc.perform(put("/users/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(gson.toJson(user)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(user.getId()))
                .andExpect(jsonPath("$.nickname").value(user.getNickname()))
                .andExpect(jsonPath("$.firstname").value(user.getFirstname()))
                .andExpect(jsonPath("$.lastname").value(user.getLastname()))
                .andExpect(jsonPath("$.midlename").value(user.getMidlename()))
                .andExpect(jsonPath("$.addInfo").value(user.getAddInfo()))
                .andExpect(jsonPath("$.city.name").value(user.getCity().getName()))
                .andExpect(jsonPath("$.contacts.length()").value(user.getContacts().size()))
                .andExpect(jsonPath("$.skills.length()").value(user.getSkills().size()));

    }

    @Test
    void updateUser_error() throws Exception {
        User user = new User("testNickname");
        user.setId(1);
        String errorText1 = "User with id " + user.getId() + " not found";
        Mockito.when(userService.update(Mockito.any(User.class))).thenThrow(new ObjectNotFoundException(errorText1));

        mockMvc.perform(put("/users/{id}", user.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(gson.toJson(user)))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(errorText1)));
    }

    @Test
    void addPictureToUser_success() throws Exception {
        User user = new User("testNickname");
        user.setId(1);
        List<Picture> pictures = new ArrayList<>();
        pictures.add(new Picture("image".getBytes(), true));
        user.setPics(pictures);
        Mockito.when(userService.addPicture(Mockito.eq(user.getId()), Mockito.any(Picture.class))).thenReturn(user.getPics());

        ResultActions resultActions = mockMvc.perform(post("/users/{id}/pic", user.getId())
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(gson.toJson(pictures.get(0))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].actual").value(true));
    }

    @Test
    void addPictureToUser_error() throws Exception {
        User user = new User("testNickname");
        user.setId(1);
        String errorText = "User with id " + user.getId() + " not found";
        Mockito.when(userService.addPicture(Mockito.eq(user.getId()), Mockito.any(Picture.class))).thenThrow(new ObjectNotFoundException(errorText));

        ResultActions resultActions = mockMvc.perform(post("/users/{id}/pic", user.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(gson.toJson(new Picture("image".getBytes(), true))))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(errorText)));
    }

    @Test
    void getActivePicture_success() throws Exception {
        User user = new User("testNickname");
        user.setId(1);
        List<Picture> pictures = new ArrayList<>();
        pictures.add(new Picture("image".getBytes(), true));
        user.setPics(pictures);
        Mockito.when(userService.getActivePicture(user.getId())).thenReturn(user.getPics().get(0));

        ResultActions resultActions = mockMvc.perform(get("/users/{id}/pic", user.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.actual").value(true));
    }

    @Test
    void getActivePicture_error() throws Exception {
        User user = new User("testNickname");
        user.setId(1);
        String errorText = "User with id " + user.getId() + " not found";
        Mockito.when(userService.getActivePicture(user.getId())).thenThrow(new ObjectNotFoundException(errorText));

        ResultActions resultActions = mockMvc.perform(get("/users/{id}/pic", user.getId()))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(errorText)));
    }

    @Test
    void createSubscriptionToUser_success() throws Exception {
        User user1 = new User("testNickname1");
        user1.setId(1);
        User user2 = new User("testNickname2");
        user2.setId(2);
        Mockito.when(userService.createSubscription(user1.getId(), user2.getId())).thenReturn(List.of(user2));

        ResultActions resultActions = mockMvc.perform(post("/users/{id}/subscriptions?toUserId={toUserId}", user1.getId(), user2.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].nickname").value(user2.getNickname()));
    }

    @Test
    void createSubscriptionToUser_error() throws Exception {
        User user1 = new User("testNickname1");
        user1.setId(1);
        User user2 = new User("testNickname2");
        user2.setId(2);
        String errorText = "User with id " + user1.getId() + " not found";
        Mockito.when(userService.createSubscription(user1.getId(), user2.getId())).thenThrow(new ObjectNotFoundException(errorText));

        ResultActions resultActions = mockMvc.perform(post("/users/{id}/subscriptions?toUserId={toUserId}", user1.getId(), user2.getId()))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(errorText)));
    }

    @Test
    void deleteSubscription_success() throws Exception {
        User user1 = new User("testNickname1");
        user1.setId(1);
        User user2 = new User("testNickname2");
        user2.setId(2);
        Mockito.when(userService.deleteSubscription(user1.getId(), user2.getId())).thenReturn(List.of());

        ResultActions resultActions = mockMvc.perform(delete("/users/{id}/subscriptions?toUserId={toUserId}", user1.getId(), user2.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(0)));
    }

    @Test
    void deleteSubscription_error() throws Exception {
        User user1 = new User("testNickname1");
        user1.setId(1);
        User user2 = new User("testNickname2");
        user2.setId(2);
        String errorText = "User with id " + user1.getId() + " not found";
        Mockito.when(userService.deleteSubscription(user1.getId(), user2.getId())).thenThrow(new ObjectNotFoundException(errorText));

        ResultActions resultActions = mockMvc.perform(delete("/users/{id}/subscriptions?toUserId={toUserId}", user1.getId(), user2.getId()))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(errorText)));
    }

    @Test
    void getSubscriptions_succces() throws Exception {
        User user1 = new User("testNickname1");
        user1.setId(1);
        User user2 = new User("testNickname2");
        user2.setId(2);
        Mockito.when(userService.getSubscriptions(user1.getId())).thenReturn(List.of(user2));

        ResultActions resultActions = mockMvc.perform(get("/users/{id}/subscriptions", user1.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].nickname").value(user2.getNickname()));
    }

    @Test
    void getSubscriptions_error() throws Exception {
        User user1 = new User("testNickname1");
        user1.setId(1);
        String errorText = "User with id " + user1.getId() + " not found";
        Mockito.when(userService.getSubscriptions(user1.getId())).thenThrow(new ObjectNotFoundException(errorText));

        ResultActions resultActions = mockMvc.perform(get("/users/{id}/subscriptions", user1.getId()))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(errorText)));
    }

    @Test
    void getSubscriptors_success() throws Exception {
        User user1 = new User("testNickname1");
        user1.setId(1);
        User user2 = new User("testNickname2");
        user2.setId(2);
        Mockito.when(userService.getSubscriptors(user1.getId())).thenReturn(List.of(user2));

        ResultActions resultActions = mockMvc.perform(get("/users/{id}/subscriptors", user1.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].nickname").value(user2.getNickname()));
    }

    @Test
    void getSubscriptors_error() throws Exception {
        User user1 = new User("testNickname1");
        user1.setId(1);
        String errorText = "User with id " + user1.getId() + " not found";
        Mockito.when(userService.getSubscriptors(user1.getId())).thenThrow(new ObjectNotFoundException(errorText));

        ResultActions resultActions = mockMvc.perform(get("/users/{id}/subscriptors", user1.getId()))
                .andExpect(status().isNotFound())
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException() instanceof ResponseStatusException))
                .andExpect(result -> Assertions.assertTrue(result.getResolvedException().getMessage().contains(errorText)));
    }

    private String readFileFromResources(String filename) throws URISyntaxException, IOException {
        URL resource = this.getClass().getResource(String.format("%s/%s", this.getClass().getSimpleName(), filename));
        byte[] bytes = Files.readAllBytes(Paths.get(resource.toURI()));
        return new String(bytes);
    }
}