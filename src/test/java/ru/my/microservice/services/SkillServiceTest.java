package ru.my.microservice.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.my.microservice.dblayer.entities.Skill;
import ru.my.microservice.dblayer.repositories.SkillRepository;
import ru.my.microservice.services.SkillService;
import ru.my.microservice.services.impl.SkillServiceImpl;

import java.util.List;

class SkillServiceTest {

    private SkillRepository skillRepository;

    @Test
    void save_notExist() {
        skillRepository = Mockito.mock(SkillRepository.class);
        Skill skillToCreate = new Skill("Java");
        Skill expectedSkill = new Skill("Java");
        expectedSkill.setId(1);
        Mockito.when(skillRepository.save(skillToCreate)).thenReturn(expectedSkill);
        Mockito.when(skillRepository.findByName(skillToCreate.getName())).thenReturn(null);
        SkillService skillDAO = new SkillServiceImpl(skillRepository);

        Skill actualSkill = skillDAO.save(skillToCreate);

        Assertions.assertEquals(expectedSkill.getId(), actualSkill.getId());
        Assertions.assertEquals(expectedSkill.getName(), actualSkill.getName());
    }

    @Test
    void save_existIdIsAbsent() {
        skillRepository = Mockito.mock(SkillRepository.class);
        Skill skillToCreate = new Skill("Java");
        Skill expectedSkill = new Skill("Java");
        expectedSkill.setId(1);
        Mockito.when(skillRepository.save(skillToCreate)).thenReturn(null);
        Mockito.when(skillRepository.findByName(skillToCreate.getName())).thenReturn(expectedSkill);
        SkillService skillDAO = new SkillServiceImpl(skillRepository);

        Skill actualSkill = skillDAO.save(skillToCreate);

        Assertions.assertEquals(expectedSkill.getId(), actualSkill.getId());
        Assertions.assertEquals(expectedSkill.getName(), actualSkill.getName());
    }

    @Test
    void save_existWithId() {
        skillRepository = Mockito.mock(SkillRepository.class);
        Skill skillToCreate = new Skill("Java");
        skillToCreate.setId(1);
        Skill expectedSkill = new Skill("Java");
        expectedSkill.setId(1);
        Mockito.when(skillRepository.getReferenceById(skillToCreate.getId())).thenReturn(expectedSkill);
        SkillService skillDAO = new SkillServiceImpl(skillRepository);

        Skill actualSkill = skillDAO.save(skillToCreate);

        Assertions.assertEquals(expectedSkill.getId(), actualSkill.getId());
        Assertions.assertEquals(expectedSkill.getName(), actualSkill.getName());
    }

    @Test
    void saveAll() {
        skillRepository = Mockito.mock(SkillRepository.class);
        Skill skill1 = new Skill("Java");
        Skill skill2 = new Skill("English");
        Skill skill3 = new Skill("Python");
        skill3.setId(3);
        Skill expectedSkill1 = new Skill("Java");
        expectedSkill1.setId(1);
        Skill expectedSkill2 = new Skill("English");
        expectedSkill2.setId(2);
        List<Skill> expectedSkills = List.of(expectedSkill1, expectedSkill2, skill3);
        Mockito.when(skillRepository.save(skill1)).thenReturn(expectedSkill1);
        Mockito.when(skillRepository.findByName(skill2.getName())).thenReturn(expectedSkill2);
        Mockito.when(skillRepository.getReferenceById(skill3.getId())).thenReturn(skill3);
        SkillService skillDAO = new SkillServiceImpl(skillRepository);

        List<Skill> actualSkills = skillDAO.saveAll(List.of(skill1, skill2, skill3));

        Assertions.assertEquals(3, actualSkills.size());
        Assertions.assertTrue(actualSkills.containsAll(expectedSkills));
    }
}