package ru.my.microservice.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.my.microservice.dblayer.entities.City;
import ru.my.microservice.dblayer.repositories.CityRepository;
import ru.my.microservice.services.CityService;
import ru.my.microservice.services.impl.CityServiceImpl;

class CityServiceTest {

    private CityRepository cityRepository;

    @Test
    void save_newCity() {
        cityRepository = Mockito.mock(CityRepository.class);
        City cityToCreate = new City("newCity");
        City expectedCity = new City("newCity");
        expectedCity.setId(1);
        Mockito.when(cityRepository.save(cityToCreate)).thenReturn(expectedCity);
        Mockito.when(cityRepository.findByName(expectedCity.getName())).thenReturn(null);
        CityService cityDAO = new CityServiceImpl(cityRepository);

        City actualCity = cityDAO.save(cityToCreate);

        Assertions.assertEquals(expectedCity.getId(), actualCity.getId());
        Assertions.assertEquals(expectedCity.getName(), actualCity.getName());

    }

    @Test
    void save_existIdIsAbsent() {
        cityRepository = Mockito.mock(CityRepository.class);
        City cityToCreate = new City("newCity");
        City expectedCity = new City("newCity");
        expectedCity.setId(1);
        Mockito.when(cityRepository.findByName(cityToCreate.getName())).thenReturn(expectedCity);
        CityService cityDAO = new CityServiceImpl(cityRepository);

        City actualCity = cityDAO.save(cityToCreate);

        Assertions.assertEquals(expectedCity.getId(), actualCity.getId());
        Assertions.assertEquals(expectedCity.getName(), actualCity.getName());

    }

    @Test
    void save_existWithId() {
        cityRepository = Mockito.mock(CityRepository.class);
        City cityToCreate = new City("newCity");
        cityToCreate.setId(1);
        City expectedCity = new City("newCity");
        expectedCity.setId(1);
        Mockito.when(cityRepository.getReferenceById(cityToCreate.getId())).thenReturn(expectedCity);
        CityService cityDAO = new CityServiceImpl(cityRepository);

        City actualCity = cityDAO.save(cityToCreate);

        Assertions.assertEquals(expectedCity.getId(), actualCity.getId());
        Assertions.assertEquals(expectedCity.getName(), actualCity.getName());

    }
}