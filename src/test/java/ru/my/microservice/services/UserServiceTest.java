package ru.my.microservice.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.my.microservice.dblayer.entities.Contact;
import ru.my.microservice.dblayer.entities.Picture;
import ru.my.microservice.dblayer.entities.User;
import ru.my.microservice.services.exceptions.ObjectNotFoundException;
import ru.my.microservice.dblayer.repositories.UserRepository;
import ru.my.microservice.services.CityService;
import ru.my.microservice.services.SkillService;
import ru.my.microservice.services.UserService;
import ru.my.microservice.services.impl.CityServiceImpl;
import ru.my.microservice.services.impl.SkillServiceImpl;
import ru.my.microservice.services.impl.UserServiceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class UserServiceTest {

    private UserRepository userRepository;
    private CityService cityService;
    private SkillService skillService;

    @Test
    void save() {
        userRepository = Mockito.mock(UserRepository.class);
        cityService = Mockito.mock(CityService.class);
        skillService = Mockito.mock(SkillService.class);
        User user = new User("user_nickname");
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("email", "ma@ma"));
        contacts.add(new Contact("phone", "12341234"));
        user.setContacts(contacts);
        Mockito.when(userRepository.save(user)).thenReturn(user);
        Mockito.when(cityService.save(user.getCity())).thenReturn(user.getCity());
        Mockito.when(skillService.saveAll(user.getSkills())).thenReturn(user.getSkills());

        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);

        User actualUser = userService.save(user);
        Assertions.assertEquals(user.getNickname(), actualUser.getNickname());
    }

    @Test
    void delete(){
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);

        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);
        try {
            userService.delete(user.getId(), true);
            Mockito.verify(userRepository).delete(user);

            userService.delete(user.getId(), false);
            Mockito.verify(userRepository).markAsDeleted(user.getId());
        } catch (ObjectNotFoundException e) {
            Assertions.fail("We mustn't get exception in this case");
        }
    }

    @Test
    void getUserById() {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));

        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);

        try {
            User actualUser = userService.getUserById(user.getId());
            Assertions.assertEquals(user.getNickname(), actualUser.getNickname());
            Assertions.assertEquals(user.getId(), actualUser.getId());
        } catch (ObjectNotFoundException e) {
            Assertions.fail("We mustn't get exception in this case");
        }
        user.setDeleted(true);

        Assertions.assertThrows(ObjectNotFoundException.class, () -> userService.getUserById(user.getId()));
    }

    @Test
    void addPicture_initialListIsNull() {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        Picture picture = new Picture("picture1".getBytes(), true);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);
        Mockito.when(userRepository.save(user)).thenReturn(user);
        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);

        try {
            List<Picture> actualPictures = userService.addPicture(user.getId(), picture);
            Assertions.assertEquals(1, actualPictures.size());
            Assertions.assertTrue(actualPictures.get(0).isActual());
        } catch (ObjectNotFoundException e) {
            Assertions.fail("We mustn't get exception in this case");
        }
    }

    @Test
    void addPicture_initialListNotNull() {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        List<Picture> pictures = new ArrayList<>();
        pictures.add(new Picture("picture1".getBytes(), true));
        user.setPics(pictures);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);
        Mockito.when(userRepository.save(user)).thenReturn(user);
        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);

        try {
            List<Picture> actualPictures = userService.addPicture(user.getId(), new Picture("picture2".getBytes(), true));
            Assertions.assertEquals(2, actualPictures.size());
            Assertions.assertTrue(actualPictures.stream().filter(p -> Arrays.equals(p.getImage(), "picture2".getBytes())).findFirst().get().isActual());
        } catch (ObjectNotFoundException e) {
            Assertions.fail("We mustn't get exception in this case");
        }
    }

    @Test
    void getActivePicture() {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        List<Picture> pictures = new ArrayList<>();
        pictures.addAll(List.of(new Picture("picture1".getBytes(), false), new Picture("picture2".getBytes(), false)));
        user.setPics(pictures);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);
        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);

        Assertions.assertThrows(ObjectNotFoundException.class, () -> userService.getActivePicture(user.getId()));
        user.getPics().add(new Picture("picture3".getBytes(), true));

        try {
            Picture activePicture = userService.getActivePicture(user.getId());
            Assertions.assertTrue(activePicture.isActual());
            Assertions.assertTrue(Arrays.equals("picture3".getBytes(), activePicture.getImage()));
        } catch (ObjectNotFoundException e) {
            Assertions.fail("We mustn't get exception in this case");

        }
    }

    @Test
    void createSubscription_startUserNotExists() {
        userRepository = Mockito.mock(UserRepository.class);
        long startUserId = 1;
        long endUserId = 2;
        Mockito.when(userRepository.existsById(startUserId)).thenReturn(false);

        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);

        Assertions.assertThrows(ObjectNotFoundException.class, () -> userService.createSubscription(startUserId, endUserId));
    }

    @Test
    void createSubscription_endUserNotExists() {
        userRepository = Mockito.mock(UserRepository.class);
        long startUserId = 1;
        long endUserId = 2;
        Mockito.when(userRepository.existsById(startUserId)).thenReturn(true);
        Mockito.when(userRepository.existsById(endUserId)).thenReturn(false);

        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);

        Assertions.assertThrows(ObjectNotFoundException.class, () -> userService.createSubscription(startUserId, endUserId));
    }

    @Test
    void getSubscriptions_none() throws ObjectNotFoundException {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);

        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);

        List<User> subscriptions = userService.getSubscriptions(user.getId());
        Assertions.assertNotNull(subscriptions);
        Assertions.assertTrue(subscriptions.isEmpty());
    }

    @Test
    void getSubscriptions_withDeletedUsers() throws ObjectNotFoundException {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname_main");
        user.setId(99);
        List<User> subscriptions = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User u = new User("user_nickname" + i);
            u.setId(i);
            if ( i % 2 == 0) {
                u.setDeleted(true);
            }
            subscriptions.add(u);
        }
        user.setSubscriptions(subscriptions);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);

        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);

        List<User> actualSubscriptions = userService.getSubscriptions(user.getId());
        Assertions.assertEquals(2, actualSubscriptions.size());
        Assertions.assertFalse(actualSubscriptions.stream().anyMatch(User::isDeleted));
    }


    @Test
    void getSubscriptors_none() throws ObjectNotFoundException {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname");
        user.setId(1);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);

        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);

        List<User> subscriptors = userService.getSubscriptors(user.getId());
        Assertions.assertNotNull(subscriptors);
        Assertions.assertTrue(subscriptors.isEmpty());
    }

    @Test
    void getSubscriptors_withDeletedUsers() throws ObjectNotFoundException {
        userRepository = Mockito.mock(UserRepository.class);
        User user = new User("user_nickname_main");
        user.setId(99);
        List<User> subscriptors = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User u = new User("user_nickname" + i);
            u.setId(i);
            if ( i % 2 == 0) {
                u.setDeleted(true);
            }
            subscriptors.add(u);
        }
        user.setSubscriptors(subscriptors);
        Mockito.when(userRepository.getReferenceById(user.getId())).thenReturn(user);

        UserService userService = new UserServiceImpl(userRepository, cityService, skillService);

        List<User> actualSubscriptors = userService.getSubscriptors(user.getId());
        Assertions.assertEquals(2, actualSubscriptors.size());
        Assertions.assertFalse(actualSubscriptors.stream().anyMatch(User::isDeleted));
    }
}