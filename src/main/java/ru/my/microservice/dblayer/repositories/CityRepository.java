package ru.my.microservice.dblayer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.my.microservice.dblayer.entities.City;

public interface CityRepository extends JpaRepository<City, Long> {
    public City findByName(String name);
}
