package ru.my.microservice.dblayer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.my.microservice.dblayer.entities.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long> {
    public Skill findByName(String name);
}
