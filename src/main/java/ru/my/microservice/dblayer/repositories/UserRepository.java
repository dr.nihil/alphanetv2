package ru.my.microservice.dblayer.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.my.microservice.dblayer.entities.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    @Modifying
    @Query("update User u set u.deleted = true where u.id = :id")
    void markAsDeleted(@Param("id") long id);

    @Query(value = "select * from \"user\" where deleted = false limit ?1 offset ?2", nativeQuery = true)
    List<User> getUsers(int max, int offset);

    @Modifying
    @Query(value = "insert into subscription values (?1, ?2)", nativeQuery = true)
    void createSubscription(long startUserId, long endUserId);

    @Modifying
    @Query(value = "delete from subscription where start_user_id = ?1 and end_user_id = ?2", nativeQuery = true)
    void deleteSubscription(long startUserId, long endUserId);

    @Override
    @Query("select count(u)>0 from User u where u.id = :id and u.deleted = false")
    boolean existsById(@Param("id") Long id);
}
