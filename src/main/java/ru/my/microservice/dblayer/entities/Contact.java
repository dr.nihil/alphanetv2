package ru.my.microservice.dblayer.entities;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name = "contact")
public class Contact {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "type")
    private String type;
    @Column(name = "value", unique = true)
    private String value;

    public Contact(String type, String value){
        this.type = type;
        this.value = value;
    }

    public Contact(){}
}
