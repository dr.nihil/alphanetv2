package ru.my.microservice.dblayer.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "pic")
@Data
public class Picture {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "image")
    private byte[] image;
    @Column(name = "is_actual")
    private boolean actual;

    public Picture(byte[] image, boolean actual) {
        this.image = image;
        this.actual = actual;
    }

    public Picture(){}
}
