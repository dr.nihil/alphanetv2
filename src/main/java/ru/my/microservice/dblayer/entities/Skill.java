package ru.my.microservice.dblayer.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "skill")
@Data
public class Skill {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "name")
    private String name;

    public Skill(String name) {
        this.name = name;
    }

    public Skill(){}

    @Override
    public boolean equals(Object object){
        if (!(object instanceof Skill)){
            return false;
        }
        Skill skill = (Skill) object;
        return skill.id == id && skill.name.equals(name);
    }

    @Override
    public int hashCode(){
        int result = Long.hashCode(id);
        result = 31 * result + name.hashCode();
        return result;
    }
}
