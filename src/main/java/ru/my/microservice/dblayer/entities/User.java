package ru.my.microservice.dblayer.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Entity
@Table(name = "\"user\"")
@Getter
@Setter
public class User {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "nickname")
    private String nickname;
    @Column(name = "birthdate")
    private Date birthdate;
    @Column(name = "firstname")
    private String firstname;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "midlename")
    private String midlename;
    @Column(name = "add_info")
    private String addInfo;
    @Column(name = "deleted")
    private boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id")
    private City city;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<Contact> contacts;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<Picture> pics;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_skills",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "skill_id")})
    private List<Skill> skills;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "subscription",
            joinColumns = {@JoinColumn(name = "end_user_id")},
            inverseJoinColumns = {@JoinColumn(name = "start_user_id")})
    @Column(updatable = false, insertable = false)
    private List<User> subscriptors;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "subscription",
            joinColumns = {@JoinColumn(name = "start_user_id")},
            inverseJoinColumns = {@JoinColumn(name = "end_user_id")})
    @Column(updatable = false, insertable = false)
    private List<User> subscriptions;

    public User(String nickname) {
        this.nickname = nickname;
    }

    public User() {}

    public void addPicture(Picture picture){
        if (pics == null){
            pics = new ArrayList<>();
        }
        if (picture.isActual()){
            pics.forEach(pic -> pic.setActual(false));
        }
        pics.add(picture);
    }

    public List<User> getSubscriptions(){
        return Optional.ofNullable(subscriptions).orElseGet(ArrayList::new).stream()
                .filter(u -> !u.isDeleted())
                .collect(Collectors.toList());
    }

    public List<User> getSubscriptors(){
        return Optional.ofNullable(subscriptors).orElseGet(ArrayList::new).stream()
                .filter(u -> !u.isDeleted())
                .collect(Collectors.toList());
    }
}
