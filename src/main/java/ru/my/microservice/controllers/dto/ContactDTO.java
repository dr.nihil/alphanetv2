package ru.my.microservice.controllers.dto;

import lombok.Data;

@Data
public class ContactDTO {
    String type;
    String value;
    public ContactDTO(){
    }

    public ContactDTO(String type, String value){
        this.type = type;
        this.value = value;
    }
}
