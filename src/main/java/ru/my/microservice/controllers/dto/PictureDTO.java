package ru.my.microservice.controllers.dto;

import lombok.Data;

@Data
public class PictureDTO {
    private byte[] image;
    private boolean actual;
}
