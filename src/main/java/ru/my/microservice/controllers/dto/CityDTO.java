package ru.my.microservice.controllers.dto;

import lombok.Data;

@Data
public class CityDTO {
    String name;
}
