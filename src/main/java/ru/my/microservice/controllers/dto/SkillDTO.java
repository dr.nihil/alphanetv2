package ru.my.microservice.controllers.dto;

import lombok.Data;

@Data
public class SkillDTO {
    String name;
    public SkillDTO(){
    }

    public SkillDTO(String name){
        this.name = name;
    }
}
