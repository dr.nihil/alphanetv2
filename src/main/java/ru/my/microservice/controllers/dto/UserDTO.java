package ru.my.microservice.controllers.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserDTO {
    private long id;
    private String nickname;
    private Date birthdate;
    private String firstname;
    private String lastname;
    private String midlename;
    private String addInfo;
    private CityDTO city;
    private List<ContactDTO> contacts;
    private List<SkillDTO> skills;
}
