package ru.my.microservice.controllers;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.my.microservice.controllers.configurations.MapperUtil;
import ru.my.microservice.controllers.dto.*;
import ru.my.microservice.dblayer.entities.*;
import ru.my.microservice.services.UserService;
import ru.my.microservice.services.exceptions.ObjectNotFoundException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {
    private final UserService userService;
    private final ModelMapper modelMapper;

    public UserController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<UserDTO> createUser(@RequestBody User user) {
        log.info("Try to create user with nickname");
        return new ResponseEntity<>(convertToUserDTO(userService.save(user)), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserDTO getUser(@PathVariable long id) {
        try {
            return convertToUserDTO(userService.getUserById(id));
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<UserDTO> getUsers(@RequestParam(required = false) Integer max, @RequestParam(required = false) Integer offset) {
        return MapperUtil.convertList(userService.getUsers(max, offset), this::convertToUserDTO);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable long id, @RequestParam(required = false) Optional<Boolean> hard) {
        try {
            userService.delete(id, hard.orElse(false));
            return new ResponseEntity<>("User with id " + id + " is deleted", HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    @ResponseBody
    public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO user, @PathVariable long id) {
        try {
            user.setId(id);
            return new ResponseEntity<>(convertToUserDTO(userService.update(convertToUser(user))), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/pic")
    public ResponseEntity<List<PictureDTO>> addPictureToUser(@RequestBody PictureDTO pic, @PathVariable long id) {
        try {
            return new ResponseEntity<>(MapperUtil.convertList(userService.addPicture(id, convertToPicture(pic)), this::convertToPictureDTO), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/pic")
    public ResponseEntity<PictureDTO> getActivePicture(@PathVariable long id) {
        try {
            return new ResponseEntity<>(convertToPictureDTO(userService.getActivePicture(id)), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/subscriptions")
    @ResponseBody
    public List<UserDTO> createSubscriptionToUser(@PathVariable long id, @RequestParam long toUserId) {
        try {
            return MapperUtil.convertList(userService.createSubscription(id, toUserId), this::convertToUserDTO);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}/subscriptions")
    public ResponseEntity<List<UserDTO>> deleteSubscription(@PathVariable long id, @RequestParam long toUserId) {
        try {
            return new ResponseEntity<>(MapperUtil.convertList(userService.deleteSubscription(id, toUserId), this::convertToUserDTO), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/subscriptions")
    public ResponseEntity<List<UserDTO>> getSubscriptions(@PathVariable long id) {
        try {
            return new ResponseEntity<>(MapperUtil.convertList(userService.getSubscriptions(id), this::convertToUserDTO), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/subscriptors")
    public ResponseEntity<List<UserDTO>> getSubscriptors(@PathVariable long id) {
        try {
            return new ResponseEntity<>(MapperUtil.convertList(userService.getSubscriptors(id), this::convertToUserDTO), HttpStatus.OK);
        } catch (ObjectNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    private User convertToUser(UserDTO userDTO) {
        if (userDTO == null){
            return null;
        }
        User user = modelMapper.map(userDTO, User.class);
        user.setCity(convertToCity(userDTO.getCity()));
        user.setContacts(MapperUtil.convertList(userDTO.getContacts(), this::convertToContact));
        user.setSkills(MapperUtil.convertList(userDTO.getSkills(), this::convertToSkill));
        return user;
    }

    private Contact convertToContact(ContactDTO contactDTO) {
        if (contactDTO == null){
            return null;
        }
        return modelMapper.map(contactDTO, Contact.class);
    }

    private Skill convertToSkill(SkillDTO skillDTO) {
        if (skillDTO == null){
            return null;
        }
        return modelMapper.map(skillDTO, Skill.class);
    }

    private City convertToCity(CityDTO cityDTO) {
        if (cityDTO == null){
            return null;
        }
        return modelMapper.map(cityDTO, City.class);
    }

    private Picture convertToPicture(PictureDTO pictureDTO) {
        if (pictureDTO == null){
            return null;
        }
        return modelMapper.map(pictureDTO, Picture.class);
    }

    private UserDTO convertToUserDTO(User user) {
        if (user == null){
            return null;
        }
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);
        userDTO.setCity(convertToCityDTO(user.getCity()));
        userDTO.setContacts(MapperUtil.convertList(user.getContacts(), this::convertToContactDTO));
        userDTO.setSkills(MapperUtil.convertList(user.getSkills(), this::convertToSkillDTO));
        return userDTO;
    }

    private ContactDTO convertToContactDTO(Contact contact) {
        if (contact == null){
            return null;
        }
        return modelMapper.map(contact, ContactDTO.class);
    }

    private SkillDTO convertToSkillDTO(Skill skill) {
        if (skill == null){
            return null;
        }
        return modelMapper.map(skill, SkillDTO.class);
    }

    private CityDTO convertToCityDTO(City city) {
        if (city == null){
            return null;
        }
        return modelMapper.map(city, CityDTO.class);
    }

    private PictureDTO convertToPictureDTO(Picture picture) {
        if (picture == null){
            return null;
        }
        return modelMapper.map(picture, PictureDTO.class);
    }
}
