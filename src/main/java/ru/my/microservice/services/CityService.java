package ru.my.microservice.services;

import ru.my.microservice.dblayer.entities.City;

public interface CityService {
    City save(City city);
}
