package ru.my.microservice.services;

import ru.my.microservice.dblayer.entities.Skill;

import java.util.List;

public interface SkillService {
    Skill save(Skill skill);
    List<Skill> saveAll(List<Skill> skills);
}
