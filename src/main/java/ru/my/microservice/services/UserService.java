package ru.my.microservice.services;

import ru.my.microservice.dblayer.entities.Picture;
import ru.my.microservice.dblayer.entities.User;
import ru.my.microservice.services.exceptions.ObjectNotFoundException;

import java.util.List;

public interface UserService {
    User save(User user);
    User getUserById(long userId) throws ObjectNotFoundException;
    List<User> getUsers(Integer max, Integer offset);
    boolean delete(long userId, boolean hard) throws ObjectNotFoundException;
    User update(User user) throws ObjectNotFoundException;
    List<Picture> addPicture(long userId, Picture pic) throws ObjectNotFoundException;
    Picture getActivePicture(long userId) throws ObjectNotFoundException;
    List<User> createSubscription(long userId, long toUserId) throws ObjectNotFoundException;
    List<User> deleteSubscription(long userId, long toUserId) throws ObjectNotFoundException;
    List<User> getSubscriptions(long userId) throws ObjectNotFoundException;
    List<User> getSubscriptors(long id) throws ObjectNotFoundException;
}
