package ru.my.microservice.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.my.microservice.dblayer.entities.Skill;
import ru.my.microservice.dblayer.repositories.SkillRepository;
import ru.my.microservice.services.SkillService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SkillServiceImpl implements SkillService {
    private final SkillRepository skillRepository;

    public SkillServiceImpl(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    public Skill save(Skill skill){
        if (skill == null) {
            return null;
        }
        if (skill.getId() != 0) {
            return skillRepository.getReferenceById(skill.getId());
        } else {
            return Optional.ofNullable(skillRepository.findByName(skill.getName()))
                    .orElseGet(() -> skillRepository.save(skill));
        }
    }

    @Transactional
    public List<Skill> saveAll(List<Skill> skills) {
        if (skills == null || skills.isEmpty()) {
            return skills;
        }
        return skills.stream()
                .map(this::save)
                .collect(Collectors.toList());
    }
}
