package ru.my.microservice.services.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.my.microservice.dblayer.entities.Picture;
import ru.my.microservice.dblayer.entities.User;
import ru.my.microservice.services.CityService;
import ru.my.microservice.services.SkillService;
import ru.my.microservice.services.UserService;
import ru.my.microservice.services.exceptions.ObjectNotFoundException;
import ru.my.microservice.dblayer.repositories.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final CityService cityService;
    private final SkillService skillService;

    private final static String USER_NOT_FOUND = "User with id %s not found";
    private final static Integer DEFAULT_MAX = 10;

    public UserServiceImpl(UserRepository userRepository, CityService cityService, SkillService skillService) {
        this.userRepository = userRepository;
        this.cityService = cityService;
        this.skillService = skillService;
    }

    @Transactional
    public User save(User user) {
        user.setSkills(skillService.saveAll(user.getSkills()));
        user.setCity(cityService.save(user.getCity()));
        return userRepository.save(user);
    }

    public User getUserById(long userId) throws ObjectNotFoundException {
        User user = userRepository.findById(userId).orElseThrow(() -> new ObjectNotFoundException(String.format(USER_NOT_FOUND, userId)));
        checkUserDeleted(user);
        return user;
    }

    public List<User> getUsers(Integer max, Integer offset) {
        List<User> users = userRepository.getUsers(Optional.ofNullable(max).orElse(DEFAULT_MAX), Optional.ofNullable(offset).orElse(0));
        return users;
    }

    @Transactional
    public boolean delete(long userId, boolean hard) throws ObjectNotFoundException {
        try {
            User user = userRepository.getReferenceById(userId);
            checkUserDeleted(user);
            if (hard) {
                userRepository.delete(user);
            } else {
                userRepository.markAsDeleted(userId);
            }
            return true;
        } catch (EntityNotFoundException e) {
            throw new ObjectNotFoundException(String.format(USER_NOT_FOUND, userId));
        }
    }

    public User update(User user) throws ObjectNotFoundException {
        try {
            User oldUser = userRepository.getReferenceById(user.getId());
            checkUserDeleted(oldUser);
            return save(user);
        } catch (EntityNotFoundException e) {
            throw new ObjectNotFoundException(String.format(USER_NOT_FOUND, user.getId()));
        }
    }

    @Transactional
    public List<Picture> addPicture(long userId, Picture pic) throws ObjectNotFoundException {
        try {
            User user = userRepository.getReferenceById(userId);
            checkUserDeleted(user);
            user.addPicture(pic);
            userRepository.save(user);
            return userRepository.getReferenceById(userId).getPics();
        } catch (EntityNotFoundException e) {
            throw new ObjectNotFoundException(String.format(USER_NOT_FOUND, userId));
        }
    }

    @Transactional
    public Picture getActivePicture(long userId) throws ObjectNotFoundException {
        try {
            User user = userRepository.getReferenceById(userId);
            checkUserDeleted(user);
            List<Picture> pictures = user.getPics();
            if (pictures == null) {
                throw new ObjectNotFoundException("User doesn't have pictures");
            }
            return pictures.stream()
                    .filter(p -> p.isActual())
                    .findFirst()
                    .orElseThrow(() -> new ObjectNotFoundException("Active picture isn't found"));
        } catch (EntityNotFoundException e) {
            throw new ObjectNotFoundException(String.format(USER_NOT_FOUND, userId));
        }
    }

    @Transactional
    public List<User> createSubscription(long startUserId, long endUserId) throws ObjectNotFoundException {
        checkUserExistsById(startUserId);
        checkUserExistsById(endUserId);
        userRepository.createSubscription(startUserId, endUserId);
        return getSubscriptions(startUserId);
    }

    @Transactional
    public List<User> deleteSubscription(long startUserId, long endUserId) throws ObjectNotFoundException {
        checkUserExistsById(startUserId);
        checkUserExistsById(endUserId);
        userRepository.deleteSubscription(startUserId, endUserId);
        return getSubscriptions(startUserId);
    }

    @Transactional
    public List<User> getSubscriptions(long userId) throws ObjectNotFoundException {
        try {
            User user = userRepository.getReferenceById(userId);
            checkUserDeleted(user);
            return user.getSubscriptions();
        } catch (EntityNotFoundException e) {
            throw new ObjectNotFoundException(String.format(USER_NOT_FOUND, userId));
        }
    }

    @Transactional
    public List<User> getSubscriptors(long userId) throws ObjectNotFoundException {
        try {
            User user = userRepository.getReferenceById(userId);
            checkUserDeleted(user);
            return user.getSubscriptors();
        } catch (EntityNotFoundException e) {
            throw new ObjectNotFoundException(String.format(USER_NOT_FOUND, userId));
        }
    }

    private void checkUserDeleted(User user) throws ObjectNotFoundException {
        if (user.isDeleted()) {
            throw new ObjectNotFoundException(String.format(USER_NOT_FOUND, user.getId()));
        }
    }

    private void checkUserExistsById(long userId) throws ObjectNotFoundException {
        if (!userRepository.existsById(userId)) {
            throw new ObjectNotFoundException(String.format(USER_NOT_FOUND, userId));
        }
    }
}
