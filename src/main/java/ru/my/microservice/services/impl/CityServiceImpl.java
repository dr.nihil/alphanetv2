package ru.my.microservice.services.impl;

import org.springframework.stereotype.Service;
import ru.my.microservice.dblayer.entities.City;
import ru.my.microservice.dblayer.repositories.CityRepository;
import ru.my.microservice.services.CityService;

import java.util.Optional;

@Service
public class CityServiceImpl implements CityService {
    private final CityRepository cityRepository;

    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public City save(City city){
        if (city == null) {
            return null;
        }
        if (city.getId() != 0) {
            return cityRepository.getReferenceById(city.getId());
        } else {
            return Optional.ofNullable(cityRepository.findByName(city.getName()))
                    .orElseGet(() -> cityRepository.save(city));
        }
    }
}
