FROM openjdk:17
RUN mkdir /app
COPY build/libs/alphanet*SNAPSHOT.jar /app/alphanet.jar
EXPOSE 8080
WORKDIR /app
CMD java $JAVA_OPTS -jar alphanet.jar